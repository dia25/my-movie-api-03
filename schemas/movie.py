from pydantic import BaseModel, Field
from typing import Optional, ClassVar, List, Dict


class Movie(BaseModel):
    id: Optional[int] = None #Indicamos que es opcional
    title: str = Field(default="Titulo de la Pelicula", min_length=2, max_length=50)
    overview: str = Field(default="Descripcion de la pelicula", min_length=2, max_length=50)
    year: int = Field(le=2024)
    rating: float = Field(ge=1, le=10)
    category: str = Field(min_length=5, max_length=15)

    movies: ClassVar[List[Dict]] = [
[
    {
        "id": 101,
        "title": "The Lord of the Rings: The Fellowship of the Ring",
        "overview": "En un exuberante planeta llamado Pandora, los habitantes enfrentan una batalla por la supervivencia.",
        "year": "2009",
        "rating": 7.8,
        "category": "Aventura"
    },
    {
        "id": 102,
        "title": "The Conjuring",
        "overview": "Una aterradora historia sobre una familia que se enfrenta a fuerzas sobrenaturales en su hogar.",
        "year": "2013",
        "rating": 9.0,
        "category": "Terror"
    }
]
]